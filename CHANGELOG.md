# Changelog

All notable changes to this project will be documented in this file.

### Links:

1. [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
2. [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
3. [Semantic commits](https://www.conventionalcommits.org/en/v1.0.0/#summary)
